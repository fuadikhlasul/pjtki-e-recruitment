<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>PJTKI Job Board - <?=$title?></title>
	<!-- Loading CSS -->
	<?php foreach ($styles as $asset) : ?>
		<link rel="stylesheet" type="text/css" href="<?=$asset?>">
	<?php endforeach; ?>
</head>
<body>

<!-- BEGIN MODAL WINDOW -->
<div class="modal fade" tabindex="-1" role="dialog" id="frmbox">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
        <h4 class="modal-title"></h4>
      </div>
      <div class="modal-body"></div>
      <div class="modal-footer"></div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<!-- END MODAL WINDOW -->

<!-- BEGIN FORM REGISTER EMPLOYEE -->
<div id="frm_employee" class="hidden">
	<form id="f_emp" action="javascript:register('employee')">
		<div class="form-group">
			<label>Nama Lengkap <span style="color: red">*</span></label>
			<input id="employee_name" name="employee_name" type="text" class="form-control" placeholder="Nama Lengkap">
		</div>
		<div class="form-group">
			<label>Nomor KTP <span style="color: red">*</span></label>
			<input id="no_ktp" name="no_ktp" type="text" class="form-control" placeholder="Nomor KTP">
		</div>
		<div class="form-group">
			<label>Alamat E-mail <span style="color: red">*</span></label>
			<input id="email" name="email" type="email" class="form-control" placeholder="Alamat E-mail">
		</div>
		<div class="form-group">
			<label>Tanggal Lahir <span style="color: red">*</span></label>
			<div class="row">
				<div class="col-md-4">
					<select name="dob_tgl" class="form-control" id="dob_tgl">
						<option value="">-- Tanggal --</option>
						<?php for ($i=1; $i<=31; $i++) { ?>
							<option><?=$i?></option>
						<?php } ?>
					</select>
				</div>
				<div class="col-md-4">
					<select name="dob_bln" class="form-control" id="dob_bln">
						<option value="">-- Bulan --</option>
						<?php for ($i=1; $i<=12; $i++) { ?>
							<option><?=$i?></option>
						<?php } ?>
					</select>
				</div>
				<div class="col-md-4">
					<select name="dob_thn" class="form-control" id="dob_thn">
						<option value="">-- Tahun --</option>
						<?php for ($i=date('Y')-50; $i<=date('Y')-17; $i++) { ?>
							<option><?=$i?></option>
						<?php } ?>
					</select>
				</div>
			</div>
		</div>
		<div class="form-group">
			<label>Username <span style="color: red">*</span></label>
			<input id="username" name="username" type="text" class="form-control" placeholder="Username">
		</div>
		<div class="form-group">
			<label>Password <span style="color: red">*</span></label>
			<input id="password" name="password" type="password" class="form-control" placeholder="Password">
		</div>
	</form>
</div>
<!-- END FORM REGISTER EMPLOYEE -->

<!-- BEGIN FORM REGISTER AGENCY -->
<div id="frm_agency" class="hidden">
	<form id="f_agency" action="javascript:register('agency')">
		<div class="form-group">
			<label>Nama Agency <span style="color: red">*</span></label>
			<input id="agency_name" name="agency_name" type="text" class="form-control" placeholder="Nama Agency">
		</div>
		<div class="form-group">
			<label>Alamat <span style="color: red">*</span></label>
			<textarea id="address" name="address" class="form-control"></textarea>
		</div>
		<div class="form-group">
			<label>Kota <span style="color: red">*</span></label>
			<input id="city" name="city" type="text" class="form-control" placeholder="Kota Agency">
		</div>
		<div class="form-group">
			<label>Username <span style="color: red">*</span></label>
			<input id="username" name="username" type="text" class="form-control" placeholder="Username">
		</div>
		<div class="form-group">
			<label>Password <span style="color: red">*</span></label>
			<input id="password" name="password" type="password" class="form-control" placeholder="Password">
		</div>
	</form>
</div>
<!-- END FORM REGISTER AGENCY -->

<!-- BEGIN FORM LOGIN -->
<div id="frm_login" class="hidden">
	<form id="f_login" action="javascript:login()">
		<div class="form-group">
			<label>Username <span style="color: red">*</span></label>
			<input id="username" name="username" type="text" class="form-control" placeholder="Username">
		</div>
		<div class="form-group">
			<label>Password <span style="color: red">*</span></label>
			<input id="password" name="password" type="password" class="form-control" placeholder="Password">
		</div>
	</form>
	<p>
		<span style="color: darkblue">Belum punya akun?</span>
		<span class="pull-right">
			<a href="javascript:_register('employee')" class="btn btn-sm btn-primary">Daftar sebagai calon TKI</a>
			<a href="javascript:_register('agency')" class="btn btn-sm btn-primary">Daftar sebagai aency</a>
		</span>
	</p>
</div>
<!-- END FORM LOGIN -->

<!-- BEGIN NAVBAR MENU -->
<nav class="navbar navbar-default navbar-fixed-top">
  <div class="container-fluid">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#main-navbar" aria-expanded="false">
        <span class="sr-only">Toggle Navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="#"><b>PJTKI</b>-JO<u>B</u>oard</a>
    </div>
    <div class="collapse navbar-collapse" id="main-navbar">
    	<ul class="nav navbar-nav">
    		<li id="home"><a href="home">Home</a></li>
        	<li id="about"><a href="#">About Us</a></li>
        	<li id="contact"><a href="#">Contact Us</a></li>
    	</ul>
    	<ul class="nav navbar-nav navbar-right">
    		<!-- <li><a href="#">Register</a></li> -->
    		<li><a href="javascript:_login()"><i class="glyphicon glyphicon-user"></i> Login</a></li>
    	</ul>
    </div>
  </div>
</nav>
<!-- END NAVBAR MENU -->

<div id="wrapper" style="background: #eee">
	<div class="container-fluid" style="padding: 0; margin: 0">
		<div class="jumbotron" 
			 style=
			 	"background-image: url('<?=base_url()?>assets/img/frontend/carousel1.jpg');
			 	 background-size: cover;
			 	 border-radius: 0">
			<div class="row">
				<div class="col-md-3" style="background: #eee">
					<h1>PJTKI JO<u>B</u>oard</h1>
					<p>Kami mempertemukan berbagai calon Tenaga Kerja Indonesia (TKI) dengan 
					berbagai <em>agency</em> yang ada di seluruh Indonesia</p>
					<p>
						<a href="javascript:_register('employee')" class="btn btn-primary btn-block">Daftar Sebagai Calon TKI</a>
						<a href="javascript:_register('agency')" class="btn btn-primary btn-block">Daftar Sebagai Agency</a>
					</p>
				</div>
			</div>
			<p><br><br><br></p>
			<div style="height: 5px"></div>
		</div>
	</div>
</div>

</body>
<!-- Loading Scripts -->
<?php foreach ($scripts as $script) : ?>
	<script type="text/javascript" src="<?=$script?>"></script>
<?php endforeach; ?>
<script type="text/javascript">

var current = '<?=$this->uri->segment(1)?>';
if ('' == current) {
	current = 'home';
}
$('#'+current).addClass('active');

function _register(role) {
	var trole = ('employee' == role) ? 'Calon TKI' : 'Agency';
	$('.modal-title').text('Form Pendaftaran '+trole);
	$('.modal-body').html($('#frm_'+role).html());
	$('.modal-footer').html(''
		+'<button type="button" data-dismiss="modal" class="btn btn-default">Batal</button>'
		+'<button type="button" onclick="register(\''+role+'\')" class="btn btn-success">Submit</button>'
	+'');
	$('#frmbox').modal();
}

function register(role) {
	var frm = ('employee' == role) ? 'emp' : 'agency';
	$.post(role+'/user/insert', $('#f_'+frm).serialize(), function(ret) {
		if ('ok;' == ret.status) {
			window.location.href = role+"/user/index/"+ret.user_id;
		} else if ('failed;' == ret.status) {
			alert('Internal Server Error. Please report to our team.');
		}
	}, 'json');
}

function _login() {
	$('.modal-title').text('Login Pengguna');
	$('.modal-body').html($('#frm_login').html());
	$('.modal-footer').html(''
		+'<button type="button" data-dismiss="modal" class="btn btn-default">Batal</button>'
		+'<button type="button" onclick="login()" class="btn btn-success">Login</button>'
	+'');
	$('#frmbox').modal();
}

function login() {

}

</script>
</html>