<!DOCTYPE html>
<html>
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1.0">
	<title>e-PJTKI | <?=$title?></title>
	<base href="<?= base_url() ?>"></base>
	<!-- Load CSS -->
	<?php foreach ($styles as $css): ?>
		<link rel="stylesheet" type="text/css" href="<?=$css?>">
	<?php endforeach; ?>
</head>

<body class="hold-transition skin-blue-light sidebar-mini">
	<div class="wrapper">

	  <!-- Main Header -->
	  <header class="main-header">

	    <!-- Logo -->
	    <a href="employee/dashboard" class="logo">
	      <!-- mini logo for sidebar mini 50x50 pixels -->
	      <span class="logo-mini"></span>
	      <!-- logo for regular state and mobile devices -->
	      <span class="logo-lg"><b>PJTKI</b> e-Recruitment</span>
	    </a>

	    <!-- Header Navbar -->
	    <nav class="navbar navbar-static-top" role="navigation">
	      <!-- Sidebar toggle button-->
	      <a href="#" class="sidebar-toggle" data-toggle="offcanvas" role="button">
	        <span class="sr-only">Toggle Navigation</span>
	      </a>
	      <!-- Navbar Right Menu -->
	      <div class="navbar-custom-menu">
	        <ul class="nav navbar-nav">
	          <!-- User Account Menu -->
	          <li class="dropdown user user-menu">
	            <!-- Menu Toggle Button -->
	            <a href="#" class="dropdown-toggle" data-toggle="dropdown">
	              <!-- The user image in the navbar-->
	              <!-- <img src="dist/img/user2-160x160.jpg" class="user-image" alt="User Image"> -->
	              <!-- hidden-xs hides the username on small devices so only the image appears. -->
	              <span class="hidden-xs"><?=$userinfo->employee_name?></span>
	            </a>
	            <ul class="dropdown-menu">
	              <!-- The user image in the menu -->
	              <li class="user-header">
	                <!-- <img src="dist/img/user2-160x160.jpg" class="img-circle" alt="User Image"> -->
	                <p>
	                  <?=$userinfo->employee_name?> - Calon TKI
	                  <small>Tanggal bergabung</small>
	                </p>
	              </li>
	              <li class="user-footer">
	                <div class="pull-right">
	                  <a href="employee/dashboard/logout" class="btn btn-danger btn-flat">Log Out</a>
	                </div>
	              </li>
	            </ul>
	          </li>
	          <!-- Control Sidebar Toggle Button -->
	          <li>
	            <a href="#" data-toggle="control-sidebar"><i class="fa fa-gears"></i></a>
	          </li>
	        </ul>
	      </div>
	    </nav>
	  </header>
	  <!-- Left side column. contains the logo and sidebar -->
	  <aside class="main-sidebar">

	    <!-- sidebar: style can be found in sidebar.less -->
	    <section class="sidebar">

	      <!-- Sidebar user panel (optional) -->
	      <div class="user-panel">
	        <div class="pull-left image">
	          <img src="#" class="img-circle" alt="User Image">
	        </div>
	        <div class="pull-left info">
	          <p><?=$userinfo->employee_name?></p>
	          <!-- Status -->
	          <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
	        </div>
	      </div>

	      <!-- Sidebar Menu -->
	      <ul class="sidebar-menu">
	        <li class="header">MENU UTAMA</li>
	        <!-- Optionally, you can add icons to the links -->
	        <li id=""><a href="#"><span>Dashboard</span></a></li>
	        <li id=""><a href="#"><span>Biodata</span></a></li>
	      </ul><!-- /.sidebar-menu -->
	    </section>
	    <!-- /.sidebar -->
	  </aside>

	  <!-- Content Wrapper. Contains page content -->
	  <div class="content-wrapper">
	    <!-- Content Header (Page header) -->
	    <section class="content-header">
	      <h1>
	        <?=$title?>
	        <small><?=$subtitle?></small>
	      </h1>
	      <ol class="breadcrumb">
	        <li><a href="#"><i class="fa fa-dashboard"></i> Level</a></li>
	        <li class="active">Here</li>
	      </ol>
	    </section>

	    <!-- Main content -->
	    <section class="content">

	      <!-- Your Page Content Here -->

	    </section><!-- /.content -->
	  </div><!-- /.content-wrapper -->

	  <!-- Main Footer -->
	  <footer class="main-footer">
	    <!-- To the right -->
	    <div class="pull-right hidden-xs">
	      Versi 1.0
	    </div>
	    <!-- Default to the left -->
	    <strong>Copyright &copy; 2016 <a href="#">BitSolutionID</a></strong> All rights reserved.
	  </footer>

	  <!-- Control Sidebar -->
	  <aside class="control-sidebar control-sidebar-dark">
	    <!-- Create the tabs -->
	    <ul class="nav nav-tabs nav-justified control-sidebar-tabs">
	      <li class="active"><a href="#control-sidebar-home-tab" data-toggle="tab"><i class="fa fa-home"></i></a></li>
	      <li><a href="#control-sidebar-settings-tab" data-toggle="tab"><i class="fa fa-gears"></i></a></li>
	    </ul>
	    <!-- Tab panes -->
	    <div class="tab-content">
	      <!-- Home tab content -->
	      <div class="tab-pane active" id="control-sidebar-home-tab">
	        <h3 class="control-sidebar-heading">Recent Activity</h3>
	        <ul class="control-sidebar-menu">
	          <li>
	            <a href="javascript::;">
	              <i class="menu-icon fa fa-birthday-cake bg-red"></i>
	              <div class="menu-info">
	                <h4 class="control-sidebar-subheading">Langdon's Birthday</h4>
	                <p>Will be 23 on April 24th</p>
	              </div>
	            </a>
	          </li>
	        </ul><!-- /.control-sidebar-menu -->

	        <h3 class="control-sidebar-heading">Tasks Progress</h3>
	        <ul class="control-sidebar-menu">
	          <li>
	            <a href="javascript::;">
	              <h4 class="control-sidebar-subheading">
	                Custom Template Design
	                <span class="label label-danger pull-right">70%</span>
	              </h4>
	              <div class="progress progress-xxs">
	                <div class="progress-bar progress-bar-danger" style="width: 70%"></div>
	              </div>
	            </a>
	          </li>
	        </ul><!-- /.control-sidebar-menu -->

	      </div><!-- /.tab-pane -->
	      <!-- Stats tab content -->
	      <div class="tab-pane" id="control-sidebar-stats-tab">Stats Tab Content</div><!-- /.tab-pane -->
	      <!-- Settings tab content -->
	      <div class="tab-pane" id="control-sidebar-settings-tab">
	        <form method="post">
	          <h3 class="control-sidebar-heading">General Settings</h3>
	          <div class="form-group">
	            <label class="control-sidebar-subheading">
	              Report panel usage
	              <input type="checkbox" class="pull-right" checked>
	            </label>
	            <p>
	              Some information about this general settings option
	            </p>
	          </div><!-- /.form-group -->
	        </form>
	      </div><!-- /.tab-pane -->
	    </div>
	  </aside><!-- /.control-sidebar -->
	  <!-- Add the sidebar's background. This div must be placed
	       immediately after the control sidebar -->
	  <div class="control-sidebar-bg"></div>
	</div><!-- ./wrapper -->
</body>

<!-- Load all scripts -->
<?php foreach ($scripts as $js): ?>
	<script type="text/javascript" src="<?=$js?>"></script>
<?php endforeach; ?>

</html>