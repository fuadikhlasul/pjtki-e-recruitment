<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Home extends CI_Controller {
	function index() {
		$this->load->view(
			'tpl',
			array(
				'title' 	=> 'Home',
				'view'		=> '_home',
				'styles'	=> array(base_url().'assets/bootstrap/css/bootstrap.min.css'),
				'scripts'	=> array(
					base_url().'assets/bootstrap/js/jquery-1.12.4.min.js', 
					base_url().'assets/bootstrap/js/bootstrap.min.js'
				)
			)
		);
	}
}