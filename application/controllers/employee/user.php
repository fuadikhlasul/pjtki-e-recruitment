<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User extends CI_Controller {
	function __construct() {
		parent::__construct();
		$this->load->model('m_employee', 'dm', TRUE);
	}

	function index($id=0) {
		if (0 != $id) {
			$this->load->view(
				'employee/tpl',
				array(
					'title' 	=> 'Biodata',
					'subtitle'	=> 'Daftar Riwayat Hidup',
					'userinfo' 	=> $this->dm->getbyid($id),
					'view' 		=> '_biodata',
					'styles'	=> array(
						base_url().'assets/adminlte/bootstrap/css/bootstrap.min.css',
						base_url().'assets/adminlte/dist/css/AdminLTE.min.css',
						base_url().'assets/adminlte/dist/css/skins/skin-blue-light.min.css'
					),
					'scripts'	=> array(
						base_url().'assets/adminlte/plugins/jQuery/jQuery-2.1.4.min.js',
						base_url().'assets/adminlte/bootstrap/js/bootstrap.min.js',
						base_url().'assets/adminlte/plugins/slimScroll/jquery.slimscroll.min.js',
						base_url().'assets/adminlte/plugins/fastclick/fastclick.min.js',
						base_url().'assets/adminlte/dist/js/app.min.js'
					)
				)
			);
		} else {

		}
	}

	function insert() {
		if(!$this->input->is_ajax_request()) return false;
		if(!$this->input->post()) return false;

		$item = $this->input->post();
		
		$month = '' . $item['dob_bln'];
		$date = '' . $item['dob_tgl'];

		if (1 == strlen($month))
			$month = '0'.$month;
		if (1 == strlen($date))
			$date = '0'.$date;

		$item['tgl_lahir'] = $item['dob_thn'] . '-' . $month . '-' . $date;
		unset($month, $date, $item['dob_thn'], $item['dob_tgl'], $item['dob_bln']);
		$item['password'] = $this->BaseModel->hash_pass($item['password']);

		if ($this->dm->add($item)) {
			$this->session->set_userdata(array(
				'is_login' => TRUE,
				'userinfo' => $item,
			));
			echo json_encode(array(
				'status'  => 'ok;',
				'user_id' => $this->db->insert_id()
			));
		} else {
			echo json_encode(array('status' => 'failed;'));
		}
	}
}