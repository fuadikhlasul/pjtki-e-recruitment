<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class M_employee extends CI_Model {
	private $table = 'employee';
	private $id = 'employee_id';

	function add($data) {
		return $this->db->insert($this->table, $data);
	}

	function getbyid($id) {
		return $this->db->get_where($this->table, array($this->id => $id))->row();
	}

	function getall() {
		return $this->db->get($this->table);
	}
}