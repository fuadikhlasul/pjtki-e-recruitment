<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class BaseModel extends CI_Model {
	function __construct() {
		parent::__construct();
		date_default_timezone_set('Asia/Jakarta');
	}

	function hash_pass($pass) {
		return substr(sha1(md5(md5($pass))), 0, 25);
	}
}